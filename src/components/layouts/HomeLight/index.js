import Header from "./Header";
import Banner from "./Banner";
import About from "./About";
import Specilizing from "./Specilizing";
import Education from "./Education";
import CaseStudy from "./CaseStudy";
import Footer from "./Footer";
import Testimonial from "./Testimonial";

export {
  Header,
  Banner,
  About,
  Specilizing,
  Education,
  CaseStudy,
  Footer,
  Testimonial,
};
