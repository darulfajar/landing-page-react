import Header from "./Header";
import Banner from "./Banner";
import Testimonial from "./Testimonial";
import ShapeIcon from "./ShapeIcon";
import About from "./About";
import Specilizing from "./Specilizing";
import Portfolio from "./Portfolio";
import Education from "./Education";
import Footer from "./Footer";
import Services from "./Services";
import CaseStudy from "./CaseStudy";
import SliderImage from "./SliderImage";
import Project from "./Project";

export {
  Header,
  Banner,
  About,
  Services,
  Education,
  CaseStudy,
  Testimonial,
  SliderImage,
  Footer,
  ShapeIcon,
  Specilizing,
  Portfolio,
  Project,
};
