import React from "react";
import HomeDark from "./HomeDark";
import HomeLight from "./HomeLight";

function Home() {
  if (
    window.matchMedia &&
    window.matchMedia("(prefers-color-scheme: dark)").matches
  ) {
    return <HomeDark />;
  } else {
    return <HomeLight />;
  }
}

export default Home;
