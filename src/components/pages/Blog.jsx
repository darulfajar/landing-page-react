import React from "react";
import {
  Banner,
  ContentBlog,
  Footer,
  Sidebar,
  Header,
} from "../layouts/blog/index";

function Blog() {
   if (
     window.matchMedia &&
     window.matchMedia("(prefers-color-scheme: dark)").matches
   ) {
     return (
       <div className="counter-scroll">
         <div id="mobile-menu-overlay">
           <span className="tf-close"></span>
         </div>
         <Header />
         <Banner />
         <div className="blog-dark col-blog">
           <div className="container d-lg-flex">
             <ContentBlog />
             <div className="col-right">
               <Sidebar />
             </div>
           </div>
         </div>
         <Footer />
       </div>
     );
   } else {
     return (
       <div className="counter-scroll">
         <div id="mobile-menu-overlay">
           <span className="tf-close"></span>
         </div>
         <Header />
         <Banner />
         <div className="blog col-blog">
           <div className="container d-lg-flex">
             <ContentBlog />
             <div className="col-right">
               <Sidebar />
             </div>
           </div>
         </div>
         <Footer />
       </div>
     );
   }
}

export default Blog;
